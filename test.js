var assert = require('assert'),
	sequenceSplitter = require('./sequenceSplitter');

describe('sequenceSplitter', function() {
	var tests = [
		{in: [1,2,3,4,5,6,7,8], out: "1-8"},
		{in: [1,3,4,5,6,7,8], out: "1,3-8"},
		{in: [1,3,4,5,6,7,8,10,11,12], out: "1,3-8,10-12"},
		{in: [1,2,3], out: "1-3"},
		{in: [1,2], out: "1,2"},
		{in: [1,2,4], out: "1,2,4"},
		{in: [1,2,4,5,6], out: "1,2,4-6"},
		{in: [1,2,3,7,8,9,15,17,19,20,21], out: "1-3,7-9,15,17,19-21"},
		{in: [1,2,3,4,5,6,100,1091,1999,2000,2001,2002,2005], out: "1-6,100,1091,1999-2002,2005"},
		{in: [1], out: "1"},
		{in: [1,3,5,7,9,11], out: "1,3,5,7,9,11"}
	];

	tests.forEach(function(test){
		it(test.in+' -> '+test.out, function(done) {
			return sequenceSplitter.run(test.in, function(result){
				assert.equal(result, test.out);
				done();
			});
		});
	});
});