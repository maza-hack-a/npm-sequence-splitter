# NodeJS Модуль #

Преобразование последовательности чисел в массиве в строку

# Установка #
    npm install npm-sequence-splitter

# Использование #

```
#!JavaScript

sequenceSplitter = require('npm-sequence-splitter');
sequenceSplitter.run([1,2,3,5,7,8], function(result){
    console.log(result);
});
```