exports.run = function(arr, callback) {
	var sequence = {
			start: 0,
			counter: 0,
			commit: function(finish){
				var str = "";
				if (sequence.counter > 1)
					str = arr[sequence.start] + '-' + arr[sequence.start+sequence.counter];
				else if (sequence.counter == 1)
					str = arr[sequence.start] + ',' + arr[sequence.start+sequence.counter];
				else
					str = arr[sequence.start];
				if (!finish)
					str += ',';
				return str;
			}
		},
		result = "";

	for(var i=0; i<arr.length; i++) {
		if (!(i && arr[i]-1 == arr[i-1])) {
			if (i) {
				result += sequence.commit();
				sequence.start = i;
				sequence.counter = 0;
			}
		}else sequence.counter++;
		if (i == arr.length-1)
			result += sequence.commit(true);
	}

	callback(result);
}